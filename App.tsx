/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useEffect, useRef} from 'react';
import {BackHandler} from 'react-native';
import WebView from 'react-native-webview';

function App(): React.JSX.Element {
  const webViewRef = useRef<null | WebView>(null);

  const handleBackButtonPress = () => {
    try {
      webViewRef.current?.goBack();

      return true;
    } catch (err: any) {
      console.log('[handleBackButtonPress] Error : ', err.message);
    }
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonPress);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonPress,
      );
    };
  }, []);

  return (
    <WebView
      ref={webViewRef}
      source={{uri: 'https://dev-fe-olshop.berkatsoft.com/'}}
    />
  );
}

export default App;
